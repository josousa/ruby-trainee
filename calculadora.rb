require_relative 'operacoes_extras'
require 'net/http'
require 'json'

module Calculadora

  class Operacoes
    include OperacoesExtras

    def media_preconceituosa(notas, lista_negra)
      json_notas=JSON.parse(notas)
      array_lista_negra=lista_negra.split(" ")
      notas_sem_lista_negra = json_notas.select do |k,v|
                            not array_lista_negra.include? k
      end
      soma=0                                        
      array_soma=notas_sem_lista_negra.map do |k,v| 
        soma=soma+v
      end
      soma = array_soma.last 
      media = soma.to_f/notas_sem_lista_negra.length 
      puts ""
      puts "A média calculada agora é igual a #{media} (hihihi)."
      puts "" 
    end






    def sem_numeros(numeros)
      array_numeros = numeros.split(" ") 
      array_numeros.map! do |n| 
        if n=="0" 
          n="00"  
        else
          n=n
        end
      end
      array_numeros.map! do |n| 
        if (n[n.length-2..-1].include?"00") || (n[n.length-2..-1].include?"25") || (n[n.length-2..-1].include?"50") || (n[n.length-2..-1].include?"75")
          n = "S" 
        else 
          n = "N"
        end
      end
      array_numeros.each do |n|
      print "#{n} " 
      end
      puts " "
    end


























    def filtrar_filmes(generos, ano)
      filmes = get_filmes
      json_generos=JSON.parse(generos)
      filmes_generos_totais=[]
      w=0
      while w<json_generos.length 
      filmes_generos = filmes[:movies].select do |k, v| 
                       k[:genres].include?json_generos[w] 
      end                                                 
      filmes_generos_ano = filmes_generos.select do |k,v| 
                           k[:year].to_i>=ano             
      end
      k=0
      while k< filmes_generos_ano.length                  
        if not filmes_generos_totais.include?filmes_generos_ano[k][:title] 
      filmes_generos_totais.push(filmes_generos_ano[k][:title])  
      end
      k=k+1
    end
      w=w+1
    end     
    if filmes_generos_totais.length == 0 
      puts ""
      puts "Nenhum filme encontrado"
      puts ""
    else
      puts ""
      puts "Filmes lançados apartir de #{ano}, relacionados aos generos digitados: "
      puts ""
      filmes_generos_totais.each do |k| 
      puts k
      end
      end
    end







   


















    def get_filmes
      url = 'https://raw.githubusercontent.com/yegor-sytnyk/movies-list/master/db.json';
      uri = URI(url);
      response = Net::HTTP.get(uri);
      return JSON.parse(response, symbolize_names: true);
    end
  end





  class Menu
    include OperacoesExtras
    
    def initialize
      a=Operacoes.new
      print "¬¬¬¬¬¬¬¬¬¬¬¬¬ SELECIONE  UMA  OPÇÃO ¬¬¬¬¬¬¬¬¬¬¬¬¬ \n";
      print "|| 1 - FILTRO DE FILMES                        || \n";
      print "|| 2 - MEDIA PRECONCEITUOSA                    || \n";
      print "|| 3 - DIVISIVEL POR 25                        || \n";
      print "|| 4 - IMPAR OU PAR?                           || \n";
      print "||                                             || \n";
      print "||                                             || \n";
      print "||                                       0-SAIR|| \n";
      print "||||||||||||||||||||||||||||||||||||||||||||||||| \n";
      option = gets.chomp.to_i



      case option
      when 1
        puts ""
        puts ""
        puts "Filtro de filmes ... on"
        puts ""
        puts "Digite o(s) genêro(s) do(s) filme(s) para a pesquisa:"
        puts "!!EXEMPLO!!"
        puts "['Genero1', 'Genero2', 'Genero3']"
        puts ""
        puts "OBS: Com aspas duplas no lugar das simples
              Gêneros devem ser em inglês"
        generos = gets.chomp
        puts ""
        puts "Digite o ano que o filtro pegara deste para diante:"
        puts ""
        ano = gets.chomp.to_i
        a.filtrar_filmes(generos,ano)





      when 2
        puts ""
        puts ""
        puts "Media preconceituosa ... on"
        puts ""
        puts "Digite a lista de alunos e suas respectivas notas nota "
        puts "!!EXEMPLO!!"
        puts "{'nome_do_aluno1':nota,'nome_do_aluno2':nota2}"
        puts ""
        puts "OBS: Com aspas duplas no lugar das simples"
        var1= gets.chomp
        puts "Digite o nome dos alunos a serem retirados da media (hihihi)"
        puts ""
        puts "OBS: Os nomes devem ser separados por espaço"
        var2= gets.chomp
        a.media_preconceituosa(var1,var2)



      when 3
        puts ""
        puts ""
        puts "Método de divisível por 25 ... on"
        puts ""
        puts "Digite o(s) numero(s) para verificar se são divisíveis por 25"
        puts "!!EXEMPLO!!"
        puts "123 634 7675 234"
        puts ""
        puts "OBS: Todos os números devem ser separados por espaço"
        numeros = gets.chomp
        a.sem_numeros(numeros)

      when 4
        puts ""
        puts ""
        puts "Impar ou par? ... on"
        puts ""
        puts "Digite o(s) número(s) para verificar se são ímpares ou pares"
        puts ""
        puts "OBS: Todos os números devem ser separados por espaço"
        num = gets.chomp
        Operacoes.new.impoupar(num)


      when 0
        puts "sair"
        exit
      else
        puts "opção invalida"
      end
    end
  end
end