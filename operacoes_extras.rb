module OperacoesExtras

    def impoupar (numbers)
    array_numbers = numbers.split(" ")

    array_numbers.map! do |n|
        if(n[n.length-1].include?"0") || (n[n.length-1].include?"2") || (n[n.length-1].include?"4") || (n[n.length-1].include?"6") || (n[n.length-1].include?"8")
            n = "PAR"
        else
            n = "IMPAR"
        end
    end

    array_numbers.each do |n|
        print "#{n} "
        puts ""
    end

    end
end
